﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IdentityMVC.Data;
using IdentityMVC.Models;
using Microsoft.AspNetCore.Authorization;

namespace IdentityMVC.Controllers
{
    [Authorize(Roles ="Admin, User")]
    public class CitiesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CitiesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Cities
        [Authorize(Roles = "Admin, User")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Cities.ToListAsync());
        }

        [Authorize(Roles = "Admin, User")]
        // GET: Cities/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var city = await _context.Cities.FirstOrDefaultAsync(m => m.Id == id);

            Country country = _context.Cities.Where(x => x.Id == id).Select(x=>x.Country).FirstOrDefault();
           
            if (country != null)
            {
                city.Country = country;
                ViewData["CountryName"] = country.Name; 
            }

            if (city == null)
            {
                return NotFound();
            }

            return View(city);
        }

        // GET: Cities/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        // POST: Cities/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description")] City city)
        {
            if (ModelState.IsValid)
            {
                _context.Add(city);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(city);
        }

        // GET: Cities/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            CityVM cityVM = new CityVM();

            cityVM.City = await _context.Cities.FindAsync(id);
            if (cityVM.City == null)
            {
                return NotFound();
            }
            //ViewData["CountryId"] 
            //var countryQuery = from c in _context.Countries
            //                   orderby c.Name
            //                   select c;
            //cityVM.SelCountries = new SelectList(countryQuery.AsNoTracking(),
            //    "CountryId", "Name", countryQuery);
            var oldCountryId = _context.Cities.Where(c => c.Id == id).Select(c => c.Country.Id).FirstOrDefault();

            var allCountries = _context.Countries;
            //var countriesQuery = allCountries.Select(c => new SelectListItem
            //{
            //    Value = c.Id.ToString(),
            //    Text = c.Name,
            //    Selected = c.Id.Equals(oldCountryId)
            //});
            //cityVM.SelCountries = countriesQuery;

            cityVM.SelectCountries = new List<Country>();
            
                foreach (var item in allCountries)
                {
                    cityVM.SelectCountries.Add(new Country 
                    { 
                        Id = item.Id,
                        Name = item.Name 
                    });
                };
            

            return View(cityVM);
        }

        // POST: Cities/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CityVM cityVM)
        {
            if (id != cityVM.Id)
            {
                return NotFound();
            }
            City city = new City();
            city = await _context.Cities.FirstOrDefaultAsync(m => m.Id == id);

            if (ModelState.IsValid)
            {
                city.Name = cityVM.City.Name;
                city.Description = cityVM.City.Description;
                //city.Country = cityVM.Country;//extrahera country av cityVM.CountryId
                if (cityVM.CountryId > 0)
                {
                    //var countryToSave = _context.Countries.Where(c => c.Id == cityVM.CountryId).FirstOrDefault();
                    var countryToSave = await _context.Countries.FirstOrDefaultAsync(m => m.Id == cityVM.CountryId);
                    city.Country = countryToSave;
                }
                //city.Country = cityVM.CountryId;
                try
                {
                    _context.Update(city);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CityExists(city.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Index)); //View(city);
        }

        // GET: Cities/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var city = await _context.Cities
                .FirstOrDefaultAsync(m => m.Id == id);
            if (city == null)
            {
                return NotFound();
            }

            return View(city);
        }

        // POST: Cities/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var city = await _context.Cities.FindAsync(id);
            _context.Cities.Remove(city);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CityExists(int id)
        {
            return _context.Cities.Any(e => e.Id == id);
        }
    }
}
