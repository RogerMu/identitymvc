﻿using System;
using System.Collections.Generic;
using System.Text;
using IdentityMVC.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IdentityMVC.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<ApplicationUser> AspNetUser { get; set; }
        //public DbSet<User> Users { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<City>().HasData(new City { Id = 1000, Name = "Borås" });
            modelBuilder.Entity<City>().HasData(new City { Id = 1002, Name = "Alingsås" });
            modelBuilder.Entity<City>().HasData(new City { Id = 1003, Name = "Helsinki" });
            modelBuilder.Entity<City>().HasData(new City { Id = 1004, Name = "Narvik" });
            modelBuilder.Entity<Country>().HasData(new Country { Id = 1005, Name = "Sverige" });
            modelBuilder.Entity<Country>().HasData(new Country { Id = 1006, Name = "Finland" });
            modelBuilder.Entity<Country>().HasData(new Country { Id = 1007, Name = "Norge" });
        }
    }
}
