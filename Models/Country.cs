﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityMVC.Data;
using IdentityMVC.Controllers;
using System.ComponentModel.DataAnnotations;

namespace IdentityMVC.Models
{
    public class Country
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
        public List<City> Cities { get; set; }
    }
}
