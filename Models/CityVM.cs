﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityMVC.Models
{
    public class CityVM
    {
        public City City { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Country Country { get; set; }
        public List<Country> Countries { get; set; }
        public IEnumerable<SelectListItem> SelCountries { get; set; }
        public List<Country> SelectCountries { get; set; }
        public int CountryId { get; set; }
    }
}
