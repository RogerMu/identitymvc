﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityMVC.Models
{
    public class ApplicationUser : IdentityUser
    {
        public City City { get; set; }
    }
}
